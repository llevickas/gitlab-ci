# gitlab-CI.yml Template

## How-To

GitLab project must have these variables filled-in, in order to start working:

* HOST: **your.hostname.com** or IP, e.g.: **0.0.0.0** (environmentally scoped)
* USER: **username** (environmentally scoped)
* SOURCE_PATH: *usually* **./**
* TARGET_PATH: **/path/where/to/deploy/at** (environmentally scoped)
* PASSWORD *if using lftp* **password** (environmentally scoped)
* SSH_PRIVATE_KEY *if using rsync* (environmentally scoped):

## RSync

```text
-----BEGIN OPENSSH PRIVATE KEY-----
The RSA key content with new lines
and all that===
-----END OPENSSH PRIVATE KEY-----
```

If using rsync, make sure the rsync is also available in the remote machine,
and the public key is already set-up.

## Excluding files & directories from uploading

Use `$EXCLUDES` env. variable to list all RegEx patterns
to ignore files from uploading.
